extern crate gl_generator;

use gl_generator::{Api, Fallbacks, GlobalGenerator, Profile, Registry};

fn main() {
    let dest = std::env::var("OUT_DIR").unwrap();
    let mut file = std::fs::File::create(&std::path::Path::new(&dest).join("bindings.rs")).unwrap();

    Registry::new(
        Api::Gl,
        (4, 5),
        Profile::Core,
        Fallbacks::All,
        [
            "GL_ARB_seamless_cubemap_per_texture",
            "GL_ARB_texture_filter_anisotropic",
            "GL_ARB_gl_spirv",
            "GL_ARB_bindless_texture",
            "GL_EXT_texture_compression_s3tc",
        ],
    ).write_bindings(GlobalGenerator, &mut file)
        .unwrap();
}
