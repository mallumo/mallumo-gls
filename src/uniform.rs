use super::*;

mod errors {
    error_chain! {
        errors {
        }
    }
}

use self::errors::*;

#[derive(Debug, Copy, Clone)]
pub enum UniformValue<'a> {
    Uniform1f(f32),
    Uniform2f(f32, f32),
    Uniform3f(f32, f32, f32),
    Uniform4f(f32, f32, f32, f32),
    Uniform1i(i32),
    Uniform2i(i32, i32),
    Uniform3i(i32, i32, i32),
    Uniform4i(i32, i32, i32, i32),
    Uniform1ui(u32),
    Uniform2ui(u32, u32),
    Uniform3ui(u32, u32, u32),
    Uniform4ui(u32, u32, u32, u32),
    Uniform1fv(&'a [f32]),
    Uniform2fv(&'a [[f32; 2]]),
    Uniform3fv(&'a [[f32; 3]]),
    Uniform4fv(&'a [[f32; 4]]),
    Uniform1iv(&'a [i32]),
    Uniform2iv(&'a [[i32; 2]]),
    Uniform3iv(&'a [[i32; 3]]),
    Uniform4iv(&'a [[i32; 4]]),
    Uniform1uiv(&'a [u32]),
    Uniform2uiv(&'a [[u32; 2]]),
    Uniform3uiv(&'a [[u32; 3]]),
    Uniform4uiv(&'a [[u32; 4]]),
    UniformMatrix2f([f32; 4]),
    UniformMatrix3f([f32; 9]),
    UniformMatrix4f([f32; 16]),
}

pub fn program_uniform(id: ProgramId, location: usize, uniform: UniformValue) -> Result<()> {
    unsafe {
        match uniform {
            UniformValue::Uniform1f(f) => program_uniform_1f(id, location, f),
            UniformValue::Uniform2f(f1, f2) => program_uniform_2f(id, location, f1, f2),
            UniformValue::Uniform3f(f1, f2, f3) => program_uniform_3f(id, location, f1, f2, f3),
            UniformValue::Uniform4f(f1, f2, f3, f4) => program_uniform_4f(id, location, f1, f2, f3, f4),
            UniformValue::Uniform1i(i) => program_uniform_1i(id, location, i),
            UniformValue::Uniform2i(i1, i2) => program_uniform_2i(id, location, i1, i2),
            UniformValue::Uniform3i(i1, i2, i3) => program_uniform_3i(id, location, i1, i2, i3),
            UniformValue::Uniform4i(i1, i2, i3, i4) => program_uniform_4i(id, location, i1, i2, i3, i4),
            UniformValue::Uniform1ui(ui) => program_uniform_1ui(id, location, ui),
            UniformValue::Uniform2ui(ui1, ui2) => program_uniform_2ui(id, location, ui1, ui2),
            UniformValue::Uniform3ui(ui1, ui2, ui3) => program_uniform_3ui(id, location, ui1, ui2, ui3),
            UniformValue::Uniform4ui(ui1, ui2, ui3, ui4) => program_uniform_4ui(id, location, ui1, ui2, ui3, ui4),
            UniformValue::Uniform1fv(fv) => program_uniform_1fv(id, location, fv),
            UniformValue::Uniform2fv(fv) => program_uniform_2fv(id, location, fv),
            UniformValue::Uniform3fv(fv) => program_uniform_3fv(id, location, fv),
            UniformValue::Uniform4fv(fv) => program_uniform_4fv(id, location, fv),
            UniformValue::Uniform1iv(iv) => program_uniform_1iv(id, location, iv),
            UniformValue::Uniform2iv(iv) => program_uniform_2iv(id, location, iv),
            UniformValue::Uniform3iv(iv) => program_uniform_3iv(id, location, iv),
            UniformValue::Uniform4iv(iv) => program_uniform_4iv(id, location, iv),
            UniformValue::Uniform1uiv(uiv) => program_uniform_1uiv(id, location, uiv),
            UniformValue::Uniform2uiv(uiv) => program_uniform_2uiv(id, location, uiv),
            UniformValue::Uniform3uiv(uiv) => program_uniform_3uiv(id, location, uiv),
            UniformValue::Uniform4uiv(uiv) => program_uniform_4uiv(id, location, uiv),
            UniformValue::UniformMatrix2f(mat2) => program_uniform_matrix_2f(id, location, mat2),
            UniformValue::UniformMatrix3f(mat3) => program_uniform_matrix_3f(id, location, mat3),
            UniformValue::UniformMatrix4f(mat4) => program_uniform_matrix_4f(id, location, mat4),
        }.chain_err(|| "Could not set uniform")
    }
}
