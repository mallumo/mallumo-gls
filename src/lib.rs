//! top level documentation
#![recursion_limit = "1024"]

#[macro_use]
extern crate bitflags;
#[macro_use]
extern crate error_chain;
extern crate glutin;

pub mod raw;
pub use raw::*;

pub mod buffer;
pub use buffer::*;

pub mod compute_program;
pub use compute_program::*;

pub mod framebuffer;
pub use framebuffer::*;

pub mod pipeline;
pub use pipeline::*;

pub mod renderer;
pub use renderer::*;

pub mod texture;
pub use texture::*;

pub mod draw;
pub use draw::*;

pub mod uniform;
pub use uniform::*;

pub mod timer_query;
pub use timer_query::*;

mod errors;
mod vertex_array_object;
