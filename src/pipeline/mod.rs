mod errors;
use errors::*;

pub mod pipeline;
pub use self::pipeline::*;

mod shader;
pub use self::shader::*;

mod spir_pipeline;
pub use self::spir_pipeline::*;

pub trait Pipeline {
    fn get_id(&self) -> ::raw::ProgramId;

    fn bind(&self) -> Result<()> {
        unsafe { ::raw::use_program(self.get_id()).chain_err(|| "Could not bind program") }
    }
}
