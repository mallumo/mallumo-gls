use super::errors::*;
use super::Pipeline;

use raw::*;

pub struct SpirPipeline {
    id: ProgramId,
}

impl Pipeline for SpirPipeline {
    fn get_id(&self) -> ProgramId {
        self.id
    }
}

impl SpirPipeline {
    pub fn new(
        vertex_shader: Option<&[u8]>,
        geometry_shader: Option<&[u8]>,
        fragment_shader: Option<&[u8]>,
    ) -> Result<SpirPipeline> {
        let program_id: ProgramId;

        unsafe {
            program_id = create_program();
        }

        let shader_types = [
            ShaderType::Vertex,
            ShaderType::Geometry,
            ShaderType::Fragment,
        ];
        unsafe {
            for (i, binary) in [vertex_shader, geometry_shader, fragment_shader]
                .iter()
                .enumerate()
            {
                if let Some(binary) = *binary {
                    let shader_id = create_shader(shader_types[i]);

                    // Load binary and specialize it
                    shader_binary(shader_id, binary).chain_err(|| "Could not compile shader binary")?;
                    specialize_shader(shader_id).chain_err(|| "Could not specialize shader")?;

                    // Check compile status
                    if !get_shader_compiled(shader_id) {
                        let shader_info_log =
                            get_shader_info_log(shader_id).chain_err(|| "Could not retrieve shader compilation log")?;

                        bail!(shader_info_log);
                    }

                    attach_shader(program_id, shader_id).chain_err(|| "Could not attach shader")?;
                }
            }

            link_program(program_id).chain_err(|| "Could not link program")?;
        }

        Ok(SpirPipeline { id: program_id })
    }

    pub fn bind(&self) -> Result<()> {
        unsafe { use_program(self.id).chain_err(|| "Could not bind program") }
    }

    pub fn get_id(&self) -> ProgramId {
        self.id
    }
}

impl Drop for SpirPipeline {
    fn drop(&mut self) {
        unsafe {
            delete_program(self.id);
        }
    }
}
