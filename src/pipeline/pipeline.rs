use super::errors::*;
use super::shader::*;

use raw::*;

pub struct Pipeline {
    id: ProgramId,
}

impl ::pipeline::Pipeline for Pipeline {
    fn get_id(&self) -> ProgramId {
        self.id
    }
}

impl<'a> ::pipeline::Pipeline for &'a Pipeline {
    fn get_id(&self) -> ProgramId {
        (*self).id
    }
}

pub struct PipelineBuilder<'a> {
    pub vertex_shader: Option<&'a Shader>,
    pub tessellation_control_shader: Option<&'a Shader>,
    pub tessellation_evaluation_shader: Option<&'a Shader>,
    pub geometry_shader: Option<&'a Shader>,
    pub fragment_shader: Option<&'a Shader>,

    // not ideal but for the time being suffices
    vertex_ok: bool,
    tessellation_control_ok: bool,
    tessellation_evaluation_ok: bool,
    geometry_ok: bool,
    fragment_ok: bool,
}

impl Pipeline {
    #[allow(unused_must_use)]
    pub fn new(
        vertex_shader: Option<&Shader>,
        tessellation_control_shader: Option<&Shader>,
        tessellation_evaluation_shader: Option<&Shader>,
        geometry_shader: Option<&Shader>,
        fragment_shader: Option<&Shader>,
    ) -> Result<Pipeline> {
        let id: ProgramId;

        unsafe {
            id = create_program();

            if let Some(ref shader) = vertex_shader {
                let attach_status = attach_shader(id, shader.get_id());
                if attach_status.is_err() {
                    delete_program(id);
                    bail!("Could not attach vertex shader");
                }
            }

            if let Some(ref shader) = tessellation_control_shader {
                let attach_status = attach_shader(id, shader.get_id());
                if attach_status.is_err() {
                    delete_program(id);
                    bail!("Could not attach tesselation control shader");
                }
            }

            if let Some(ref shader) = tessellation_evaluation_shader {
                let attach_status = attach_shader(id, shader.get_id());
                if attach_status.is_err() {
                    delete_program(id);
                    bail!("Could not attach tesselation evaluation shader");
                }
            }

            if let Some(ref shader) = geometry_shader {
                let attach_status = attach_shader(id, shader.get_id());
                if attach_status.is_err() {
                    delete_program(id);
                    bail!("Could not attach geometry shader");
                }
            }

            if let Some(ref shader) = fragment_shader {
                let attach_status = attach_shader(id, shader.get_id());
                if attach_status.is_err() {
                    delete_program(id);
                    bail!("Could not attach fragment shader");
                }
            }

            link_program(id).chain_err(|| "Could not link program")?;

            if !get_program_linked(id) {
                let program_info_log = get_program_info_log(id);
                match program_info_log {
                    Ok(log) => if log.len() > 1 {
                        delete_program(id);
                        bail!(log);
                    },
                    Err(_) => {
                        delete_program(id);
                        program_info_log.chain_err(|| "Could not get program link log")?;
                    }
                }
            }

            // ignoring error, because according to spec it cannot fail
            // because id is program object, shaders are shader objects
            //         shaders are attached to id
            if let Some(ref shader) = vertex_shader {
                detach_shader(id, shader.get_id());
            }

            if let Some(ref shader) = tessellation_control_shader {
                detach_shader(id, shader.get_id());
            }

            if let Some(ref shader) = tessellation_evaluation_shader {
                detach_shader(id, shader.get_id());
            }

            if let Some(ref shader) = geometry_shader {
                detach_shader(id, shader.get_id());
            }

            if let Some(ref shader) = fragment_shader {
                detach_shader(id, shader.get_id());
            }
        }

        Ok(Pipeline { id: id })
    }

    pub fn get_id(&self) -> ProgramId {
        self.id
    }
}

impl Drop for Pipeline {
    fn drop(&mut self) {
        unsafe {
            delete_program(self.id);
        }
    }
}
impl<'a> PipelineBuilder<'a> {
    pub fn new() -> PipelineBuilder<'a> {
        PipelineBuilder {
            vertex_shader: None,
            tessellation_control_shader: None,
            tessellation_evaluation_shader: None,
            geometry_shader: None,
            fragment_shader: None,

            vertex_ok: true,
            tessellation_control_ok: true,
            tessellation_evaluation_ok: true,
            geometry_ok: true,
            fragment_ok: true,
        }
    }

    pub fn vertex_shader(mut self, shader: &'a Shader) -> PipelineBuilder {
        if shader.shader_type != ShaderType::Vertex {
            self.vertex_ok = false;
        }

        self.vertex_shader = Some(shader);
        self
    }

    pub fn tessellation_control_shader(mut self, shader: &'a Shader) -> PipelineBuilder {
        if shader.shader_type != ShaderType::TessellationControl {
            self.tessellation_control_ok = false;
        }

        self.tessellation_control_shader = Some(shader);
        self
    }

    pub fn tessellation_evaluation_shader(mut self, shader: &'a Shader) -> PipelineBuilder {
        if shader.shader_type != ShaderType::TessellationEvaluation {
            self.tessellation_evaluation_ok = false;
        }

        self.tessellation_evaluation_shader = Some(shader);
        self
    }

    pub fn geometry_shader(mut self, shader: &'a Shader) -> PipelineBuilder {
        if shader.shader_type != ShaderType::Geometry {
            self.geometry_ok = false;
        }

        self.geometry_shader = Some(shader);
        self
    }

    pub fn fragment_shader(mut self, shader: &'a Shader) -> PipelineBuilder {
        if shader.shader_type != ShaderType::Fragment {
            self.fragment_ok = false;
        }

        self.fragment_shader = Some(shader);
        self
    }

    pub fn build(self) -> Result<Pipeline> {
        if !self.vertex_ok {
            bail!("Wrong type of shader supplied, expected vertex");
        }
        if !self.tessellation_control_ok {
            bail!("Wrong type of shader supplied, expected tessellation control");
        }
        if !self.tessellation_evaluation_ok {
            bail!("Wrong type of shader supplied, expected tessellation evaluation");
        }
        if !self.geometry_ok {
            bail!("Wrong type of shader supplied, expected geometry");
        }
        if !self.fragment_ok {
            bail!("Wrong type of shader supplied, expected fragment");
        }

        Pipeline::new(
            self.vertex_shader,
            self.tessellation_control_shader,
            self.tessellation_evaluation_shader,
            self.geometry_shader,
            self.fragment_shader,
        ).chain_err(|| "Could not build pipeline")
    }
}
