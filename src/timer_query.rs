use super::*;

pub struct TimerQuery {
    id: u32,
    elapsed_time: u64,    
}

impl TimerQuery {
    pub fn new() -> TimerQuery {
        let mut id: u32 = 0;
        unsafe {
            gl::GenQueries(1, &mut id as *mut u32);
        }

        TimerQuery {
            id: id,
            elapsed_time: 0u64,
        }
    }

    pub fn begin(&mut self) {
        unsafe {
            gl::BeginQuery(gl::TIME_ELAPSED, self.id);
        }
    }

    pub fn end(&mut self) {
        unsafe {
            gl::EndQuery(gl::TIME_ELAPSED);
        }
    }

    pub fn end_ns(&mut self) -> u64 {
        self.end();

        self.get_elapsed_time_ns()
    }

    pub fn end_ms(&mut self) -> u64 {
        self.end_ns() / 1_000_000
    }

    pub fn is_available(&self) -> bool {
        let mut available: i32 = 0;
        unsafe {
            gl::GetQueryObjectiv(self.id, gl::QUERY_RESULT_AVAILABLE, &mut available);
        }

        available != 0
    }

    pub fn get_elapsed_time_ns(&mut self) -> u64 {
        while !self.is_available() {

        }

        unsafe {
            gl::GetQueryObjectui64v(self.id, gl::QUERY_RESULT, &mut self.elapsed_time as *mut u64);
        }

        self.elapsed_time
    }

    pub fn get_elapsed_time_ms(&mut self) -> u64 {
        self.get_elapsed_time_ns() / 1_000_000
    }
}