use super::*;
use super::errors::*;

#[derive(Debug)]
pub struct Texture3D {
    id: TextureId,

    pub size: Texture3DSize,
    pub mipmap_levels: usize,

    pub internal_format: TextureInternalFormat,
    pub format: TextureFormat,
    pub data_type: TextureDataType,
}

#[derive(Debug, Copy, Clone, PartialEq)]
pub struct Texture3DSize(pub usize, pub usize, pub usize);

#[derive(Debug, Copy, Clone, PartialEq, Default)]
pub struct Texture3DParameters {
    pub min: TextureTexelFilter,
    pub mag: TextureTexelFilter,
    pub mipmap: TextureMipmapFilter,
    pub wrap_s: TextureWrapMode,
    pub wrap_t: TextureWrapMode,
    pub wrap_r: TextureWrapMode,
}

impl Texture for Texture3D {
    fn get_id(&self) -> TextureId {
        self.id
    }

    fn get_target(&self) -> TextureTarget {
        TextureTarget::Texture3D
    }
}

impl Texture3D {
    pub fn new<T: TextureDataPrimitive, M: Into<MipmapLevel>>(
        size: Texture3DSize,
        internal_format: TextureInternalFormat,
        format: TextureFormat,
        data_type: TextureDataType,
        parameters: Texture3DParameters,
        levels: M,
        data: &[T],
    ) -> Result<Texture3D> {
        if size.0 == 0 || size.1 == 0 || size.2 == 0 {
            bail!("Texture size must be greater than 0");
        }

        let mut texture = Texture3D::new_empty(size, internal_format, format, data_type, parameters, levels)
            .chain_err(|| "Could not create texture")?;

        texture
            .set_data(data)
            .chain_err(|| "Could not set data of texture")?;

        Ok(texture)
    }

    pub fn new_empty<M: Into<MipmapLevel>>(
        size: Texture3DSize,
        internal_format: TextureInternalFormat,
        format: TextureFormat,
        data_type: TextureDataType,
        parameters: Texture3DParameters,
        levels: M,
    ) -> Result<Texture3D> {
        if size.0 == 0 || size.1 == 0 || size.2 == 0 {
            bail!("Texture size must be greater than 0");
        }

        let levels = levels.into().num_levels_3d(size);

        unsafe {
            let id = create_texture(TextureTarget::Texture3D);

            let mut texture = Texture3D {
                id: id,

                size: size,
                mipmap_levels: levels,

                internal_format: internal_format,
                format: format,
                data_type: data_type,
            };

            texture_storage_3d(id, levels, internal_format, (size.0, size.1, size.2))
                .chain_err(|| "Unable to allocate memory for texture")?;

            texture
                .set_parameters(parameters)
                .chain_err(|| "Unable to set texture parameters")?;

            Ok(texture)
        }
    }

    pub fn set_data<T: TextureDataPrimitive>(&mut self, data: &[T]) -> Result<()> {
        if self.size.0 * self.size.1 * self.size.2 * self.format.n_components() != data.len() {
            bail!("Data length does not match texture size");
        }
        unsafe {
            texture_subimage_3d(
                self.id,
                0,
                (0, 0, 0),
                (self.size.0, self.size.1, self.size.2),
                self.format.into(),
                self.data_type.into(),
                data,
            ).chain_err(|| "Could not set texture data")?;
        }
        Ok(())
    }

    pub fn set_subdata<T: TextureDataPrimitive>(
        &mut self,
        size: Texture3DSize,
        offset: Texture3DSize,
        data: &[T],
    ) -> Result<()> {
        if size.0 * size.1 * size.2 * self.format.n_components() != data.len() {
            bail!("Data length does not match proclaimed size");
        }
        if size.0 + offset.0 >= self.size.0 || size.1 + offset.1 >= self.size.1 || size.2 + offset.2 >= self.size.2 {
            bail!("Size + offset is outside texture bounds");
        }
        unsafe {
            texture_subimage_3d(
                self.id,
                0,
                (offset.0, offset.1, offset.2),
                (size.0, size.1, size.2),
                self.format.into(),
                self.data_type.into(),
                data,
            ).chain_err(|| "Could not set texture data")?;
        }
        Ok(())
    }

    pub fn set_data_mipmap<T: TextureDataPrimitive>(&mut self, level: usize, data: &[T]) -> Result<()> {
        if self.mipmap_levels < level {
            bail!("Mipmap level references greater level than texture has");
        }
        if texture_size_mipmap(self.size.0, level) * texture_size_mipmap(self.size.1, level)
            * texture_size_mipmap(self.size.2, level) * self.format.n_components() != data.len()
        {
            bail!("Data length does not match texture size");
        }
        unsafe {
            texture_subimage_3d(
                self.id,
                0,
                (0, 0, 0),
                (self.size.0, self.size.1, self.size.2),
                self.format.into(),
                self.data_type.into(),
                data,
            ).chain_err(|| "Could not set texture data")?;
        }
        Ok(())
    }

    pub fn set_subdata_mipmap<T: TextureDataPrimitive>(
        &mut self,
        level: usize,
        size: Texture3DSize,
        offset: Texture3DSize,
        data: &[T],
    ) -> Result<()> {
        if self.mipmap_levels < level {
            bail!("Mipmap level references greater level than texture has");
        }
        if texture_size_mipmap(self.size.0, level) * texture_size_mipmap(self.size.1, level)
            * texture_size_mipmap(self.size.2, level) * self.format.n_components() != data.len()
        {
            bail!("Data length does not match texture size");
        }
        if size.0 + offset.0 >= texture_size_mipmap(self.size.0, level)
            || size.1 + offset.1 >= texture_size_mipmap(self.size.1, level)
            || size.2 + offset.2 >= texture_size_mipmap(self.size.2, level)
        {
            bail!("Size + offset is outside texture bounds");
        }
        unsafe {
            texture_subimage_3d(
                self.id,
                0,
                (offset.0, offset.1, offset.2),
                (size.0, size.1, size.2),
                self.format.into(),
                self.data_type.into(),
                data,
            ).chain_err(|| "Could not set texture data")?;
        }
        Ok(())
    }

    pub fn generate_mipmap(&mut self) {
        unsafe {
            generate_texture_mipmap(self.id).unwrap();
        }
    }

    pub fn clear(&mut self) -> Result<()> {
        for level in 0..self.mipmap_levels {
            self.clear_mipmap(level)
                .chain_err(|| format!("Could not clear mipmap level {}", level))?;
        }

        Ok(())
    }

    pub fn clear_mipmap(&mut self, level: usize) -> Result<()> {
        if self.mipmap_levels < level {
            bail!("Mipmap level references greater level than texture has");
        }

        unsafe {
            match clear_tex_image_null(self.id, level, self.format, self.data_type) {
                Ok(_) => Ok(()),
                Err(_) => bail!("Could not clear mipmap level"),
            }
        }
    }

    fn set_parameters(&mut self, parameters: Texture3DParameters) -> Result<()> {
        let min = TextureParameterOption::MinFilter(parameters.min, parameters.mipmap);
        let mag = TextureParameterOption::MagFilter(parameters.mag);
        let wrap_s = TextureParameterOption::WrapS(parameters.wrap_s);
        let wrap_t = TextureParameterOption::WrapT(parameters.wrap_t);
        let wrap_r = TextureParameterOption::WrapR(parameters.wrap_r);

        unsafe {
            texture_parameter_i(self.id, min).chain_err(|| "Could not set min parameter")?;
            texture_parameter_i(self.id, mag).chain_err(|| "Could not set mag parameter")?;
            texture_parameter_i(self.id, wrap_s).chain_err(|| "Could not set wrap s parameter")?;
            texture_parameter_i(self.id, wrap_t).chain_err(|| "Could not set wrap t parameter")?;
            texture_parameter_i(self.id, wrap_r).chain_err(|| "Could not set wrap r parameter")?;
        }

        Ok(())
    }
}

impl Drop for Texture3D {
    fn drop(&mut self) {
        unsafe { delete_texture(self.id) };
    }
}
