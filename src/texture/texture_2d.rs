use super::*;
use super::errors::*;

use std::cmp::max;

#[derive(Debug)]
pub struct Texture2D {
    id: TextureId,
    handle: TextureHandle,

    pub size: Texture2DSize,
    pub mipmap_levels: usize,

    pub internal_format: TextureInternalFormat,
    pub format: TextureFormat,
    pub data_type: TextureDataType,
}

#[derive(Debug, Copy, Clone, PartialEq)]
pub struct Texture2DSize(pub usize, pub usize);

#[derive(Debug, Copy, Clone, PartialEq, Default)]
pub struct Texture2DParameters {
    pub min: TextureTexelFilter,
    pub mag: TextureTexelFilter,
    pub mipmap: TextureMipmapFilter,
    pub wrap_s: TextureWrapMode,
    pub wrap_t: TextureWrapMode,
}

impl Texture for Texture2D {
    fn get_id(&self) -> TextureId {
        self.id
    }

    fn get_handle(&self) -> TextureHandle {
        self.handle
    }

    fn get_target(&self) -> TextureTarget {
        TextureTarget::Texture2D
    }
}

impl Texture2D {
    pub fn new<T: TextureDataPrimitive, M: Into<MipmapLevel>>(
        size: Texture2DSize,
        internal_format: TextureInternalFormat,
        format: TextureFormat,
        data_type: TextureDataType,
        parameters: Texture2DParameters,
        levels: M,
        data: &[T],
    ) -> Result<Texture2D> {
        if size.0 == 0 || size.1 == 0 {
            bail!("Texture size must be greater than 0");
        }

        let mut texture = Texture2D::new_empty(size, internal_format, format, data_type, parameters, levels)
            .chain_err(|| "Could not create texture")?;

        texture
            .set_data(data)
            .chain_err(|| "Could not set data of texture")?;

        Ok(texture)
    }

    pub fn new_empty<M: Into<MipmapLevel>>(
        size: Texture2DSize,
        internal_format: TextureInternalFormat,
        format: TextureFormat,
        data_type: TextureDataType,
        parameters: Texture2DParameters,
        levels: M,
    ) -> Result<Texture2D> {
        if size.0 == 0 || size.1 == 0 {
            bail!("Texture size must be greater than 0");
        }

        let mipmap_levels = levels.into().num_levels_2d(size);

        unsafe {
            let id = create_texture(TextureTarget::Texture2D);

            let mut texture = Texture2D {
                id,
                handle: TextureHandle::empty(),

                size,
                mipmap_levels,

                internal_format,
                format,
                data_type,
            };

            texture_storage_2d(id, mipmap_levels, internal_format, (size.0, size.1))
                .chain_err(|| "Unable to allocate memory for texture")?;

            //let mut anisotropy = 0.0f32;
            //gl::GetFloatv(gl::MAX_TEXTURE_MAX_ANISOTROPY, &mut anisotropy as *mut f32);
            //gl::TextureParameterf(id.into(), gl::TEXTURE_MAX_ANISOTROPY, 2.0);

            texture
                .set_parameters(parameters)
                .chain_err(|| "Could not set parameters")?;

            texture.handle = get_texture_handle(id).unwrap();
            make_texture_handle_resident(texture.handle).unwrap();

            Ok(texture)
        }
    }

    pub fn set_data<T: TextureDataPrimitive>(&mut self, data: &[T]) -> Result<()> {
        if self.size.0 * self.size.1 * self.format.n_components() != data.len() {
            bail!("Data length does not match texture size");
        }
        unsafe {
            texture_subimage_2d(
                self.id,
                0,
                (0, 0),
                (self.size.0, self.size.1),
                self.format.into(),
                self.data_type.into(),
                data,
            ).chain_err(|| "Could not set texture data")?;
        }
        Ok(())
    }

    pub fn set_subdata<T: TextureDataPrimitive>(
        &mut self,
        size: Texture2DSize,
        offset: Texture2DSize,
        data: &[T],
    ) -> Result<()> {
        if size.0 * size.1 * self.format.n_components() != data.len() {
            bail!("Data length does not match proclaimed size");
        }
        if size.0 + offset.0 >= self.size.0 || size.1 + offset.1 >= self.size.1 {
            bail!("Size + offset is outside texture bounds");
        }
        unsafe {
            texture_subimage_2d(
                self.id,
                0,
                (offset.0, offset.1),
                (size.0, size.1),
                self.format.into(),
                self.data_type.into(),
                data,
            ).chain_err(|| "Could not set texture data")?;
        }
        Ok(())
    }

    pub fn set_data_mipmap<T: TextureDataPrimitive>(&mut self, level: usize, data: &[T]) -> Result<()> {
        if discrete_floored_log2(max(self.size.0, self.size.1)) < level {
            bail!("Mipmap level references greater level than texture has");
        }
        if texture_size_mipmap(self.size.0, level) * texture_size_mipmap(self.size.1, level)
            * self.format.n_components() != data.len()
        {
            bail!("Data length does not match texture size");
        }
        unsafe {
            texture_subimage_2d(
                self.id,
                level,
                (0, 0),
                (self.size.0, self.size.1),
                self.format.into(),
                self.data_type.into(),
                data,
            ).chain_err(|| "Could not set texture data")?;
        }
        Ok(())
    }

    pub fn set_subdata_mipmap<T: TextureDataPrimitive>(
        &mut self,
        level: usize,
        size: Texture2DSize,
        offset: Texture2DSize,
        data: &[T],
    ) -> Result<()> {
        if discrete_floored_log2(max(self.size.0, self.size.1)) < level {
            bail!("Mipmap level references greater level than texture has");
        }
        if texture_size_mipmap(size.0, level) * texture_size_mipmap(size.1, level) * self.format.n_components()
            != data.len()
        {
            bail!("Data length does not match proclaimed size");
        }
        if size.0 + offset.0 >= texture_size_mipmap(self.size.0, level)
            || size.1 + offset.1 >= texture_size_mipmap(self.size.1, level)
        {
            bail!("Size + offset is outside texture bounds");
        }
        unsafe {
            texture_subimage_2d(
                self.id,
                level,
                (offset.0, offset.1),
                (size.0, size.1),
                self.format.into(),
                self.data_type.into(),
                data,
            ).chain_err(|| "Could not set texture data")?;
        }
        Ok(())
    }

    pub fn generate_mipmap(&mut self) {
        unsafe {
            generate_texture_mipmap(self.id).unwrap();
        }
    }

    pub fn clear(&mut self) -> Result<()> {
        for level in 0..self.mipmap_levels {
            self.clear_mipmap(level)
                .chain_err(|| format!("Could not clear mipmap level {}", level))?;
        }

        Ok(())
    }

    pub fn clear_mipmap(&mut self, level: usize) -> Result<()> {
        if self.mipmap_levels < level {
            bail!("Mipmap level references greater level than texture has");
        }

        unsafe {
            match clear_tex_image_null(self.id, level, self.format, self.data_type) {
                Ok(_) => Ok(()),
                Err(_) => bail!("Could not clear mipmap level"),
            }
        }
    }

    fn set_parameters(&mut self, parameters: Texture2DParameters) -> Result<()> {
        let min = TextureParameterOption::MinFilter(parameters.min, parameters.mipmap);
        let mag = TextureParameterOption::MagFilter(parameters.mag);
        let wrap_s = TextureParameterOption::WrapS(parameters.wrap_s);
        let wrap_t = TextureParameterOption::WrapT(parameters.wrap_t);

        unsafe {
            texture_parameter_i(self.id, min).chain_err(|| "Could not set min parameter")?;
            texture_parameter_i(self.id, mag).chain_err(|| "Could not set mag parameter")?;
            texture_parameter_i(self.id, wrap_s).chain_err(|| "Could not set wrap s parameter")?;
            texture_parameter_i(self.id, wrap_t).chain_err(|| "Could not set wrap t parameter")?;
        }

        Ok(())
    }
}

impl Drop for Texture2D {
    fn drop(&mut self) {
        unsafe { delete_texture(self.id) };
    }
}
