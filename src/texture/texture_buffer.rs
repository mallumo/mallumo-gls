use super::*;
use super::errors::*;
use raw::*;
use buffer::*;

use std::cell::Cell;

pub struct TextureBuffer<'a> {
    id: TextureId,

    inner_buffer: TextureBufferInner<'a>,

    internal_format: TextureBufferInternalFormat,
}

pub enum TextureBufferInner<'a> {
    OwnedImmutableBuffer(ImmutableBuffer),
    OwnedMutableBuffer(MutableBuffer),
    BorrowedImmutableBuffer(&'a ImmutableBuffer),
    ImmutablyBorrowedMutableBuffer(&'a MutableBuffer),
    MutablyBorrowedMutableBuffer(&'a mut MutableBuffer),
}

impl<'a> Texture for TextureBuffer<'a> {
    fn get_id(&self) -> TextureId {
        self.id
    }

    fn get_target(&self) -> TextureTarget {
        TextureTarget::TextureBuffer
    }
}

impl<'a> TextureBuffer<'a> {
    pub fn new(internal_format: TextureBufferInternalFormat, buffer: TextureBufferInner) -> Result<TextureBuffer> {
        unsafe {
            let id = create_texture(TextureTarget::TextureBuffer);

            let buffer_id = match buffer {
                TextureBufferInner::OwnedImmutableBuffer(ref buffer) => buffer.get_id(),
                TextureBufferInner::OwnedMutableBuffer(ref buffer) => buffer.get_id(),
                TextureBufferInner::BorrowedImmutableBuffer(ref buffer) => buffer.get_id(),
                TextureBufferInner::ImmutablyBorrowedMutableBuffer(ref buffer) => buffer.get_id(),
                TextureBufferInner::MutablyBorrowedMutableBuffer(ref buffer) => buffer.get_id(),
            };

            texture_buffer(id, internal_format, buffer_id).chain_err(|| "Unable to attach buffer to texture")?;

            let texture = TextureBuffer {
                id: id,

                inner_buffer: buffer,

                internal_format: internal_format,
            };

            Ok(texture)
        }
    }
}

impl<'a> Drop for TextureBuffer<'a> {
    fn drop(&mut self) {
        unsafe { delete_texture(self.id) };
    }
}
