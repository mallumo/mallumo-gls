mod errors;

mod texture_1d;
pub use self::texture_1d::*;

mod texture_2d;
pub use self::texture_2d::*;

mod texture_3d;
pub use self::texture_3d::*;

mod texture_cubemap;
pub use self::texture_cubemap::*;

// mod texture_buffer;
// pub use self::texture_buffer::*;

use std;
use std::cmp::max;
use raw::*;

use self::errors::*;

pub trait Texture {
    fn get_id(&self) -> TextureId;
    fn get_handle(&self) -> TextureHandle {
        TextureHandle::empty()
    }
    fn get_target(&self) -> TextureTarget;

    fn bind_texture(&self, unit: usize) {
        unsafe {
            bind_texture_unit(unit, self.get_id());
        }
    }

    fn bind_image(&self, unit: usize, level: usize, format: ImageInternalFormat) -> Result<()> {
        unsafe {
            bind_image_texture(
                unit,
                self.get_id(),
                level,
                None,
                ImageAccess::ReadWrite,
                format,
            ).chain_err(|| "Could not bind image")
        }
    }

    fn bind_image_read(&self, unit: usize, level: usize, format: ImageInternalFormat) -> Result<()> {
        unsafe {
            bind_image_texture(unit, self.get_id(), level, None, ImageAccess::Read, format)
                .chain_err(|| "Could not bind image")
        }
    }

    fn bind_image_write(&self, unit: usize, level: usize, format: ImageInternalFormat) -> Result<()> {
        unsafe {
            bind_image_texture(unit, self.get_id(), level, None, ImageAccess::Write, format)
                .chain_err(|| "Could not bind image")
        }
    }
}

pub trait TextureDataPrimitive {}

impl TextureDataPrimitive for i8 {}
impl TextureDataPrimitive for i16 {}
impl TextureDataPrimitive for i32 {}

impl TextureDataPrimitive for u8 {}
impl TextureDataPrimitive for u16 {}
impl TextureDataPrimitive for u32 {}

impl TextureDataPrimitive for f32 {}

#[derive(Debug, Copy, Clone, PartialEq)]
pub enum MipmapLevel {
    Level(usize),
    Max,
}

impl Into<MipmapLevel> for usize {
    fn into(self) -> MipmapLevel {
        MipmapLevel::Level(self)
    }
}

impl MipmapLevel {
    fn num_levels_1d(self, s: Texture1DSize) -> usize {
        match self {
            MipmapLevel::Level(l) => l,
            MipmapLevel::Max => 1 + discrete_floored_log2(s.0),
        }
    }

    fn num_levels_2d(self, s: Texture2DSize) -> usize {
        match self {
            MipmapLevel::Level(l) => l,
            MipmapLevel::Max => 1 + discrete_floored_log2(max(s.0, s.1)),
        }
    }

    fn num_levels_3d(self, s: Texture3DSize) -> usize {
        match self {
            MipmapLevel::Level(l) => l,
            MipmapLevel::Max => 1 + discrete_floored_log2(max(s.0, max(s.1, s.2))),
        }
    }

    fn num_levels_cubemap(self, s: TextureCubemapSize) -> usize {
        match self {
            MipmapLevel::Level(l) => l,
            MipmapLevel::Max => 1 + discrete_floored_log2(max(s.0, s.1)),
        }
    }
}

fn discrete_floored_log2(mut n: usize) -> usize {
    let mut ret = 0;

    while n > 1 {
        n /= 2;
        ret += 1;
    }

    ret
}

// may panic if level is too high but Result<> is not necessary as it is for internal use only
fn texture_size_mipmap(size: usize, level: usize) -> usize {
    std::cmp::max(1, size / 2u32.pow(level as u32) as usize)
}
