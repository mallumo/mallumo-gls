use raw::*;

#[derive(Debug, Copy, Clone, PartialEq, Eq)]
pub struct VertexArrayObjectId(u32);

impl From<VertexArrayObjectId> for gl::types::GLenum {
    fn from(vao_id: VertexArrayObjectId) -> Self {
        vao_id.0
    }
}

pub unsafe fn create_vertex_array() -> VertexArrayObjectId {
    let mut vertex_array_id: u32 = 0;
    let vertex_array_id_ptr: *mut u32 = &mut vertex_array_id;

    gl::CreateVertexArrays(1, vertex_array_id_ptr);

    VertexArrayObjectId(vertex_array_id)
}

pub unsafe fn bind_vertex_array(id: VertexArrayObjectId) {
    gl::BindVertexArray(id.into());
}

pub unsafe fn delete_vertex_array(id: VertexArrayObjectId) {
    gl::DeleteVertexArrays(1, &id.into());
}
