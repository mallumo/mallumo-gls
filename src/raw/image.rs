use super::*;
use super::errors::*;

#[derive(Debug, Copy, Clone, PartialEq, Eq)]
pub enum ImageAccess {
    Read,
    Write,
    ReadWrite,
}

impl From<ImageAccess> for u32 {
    fn from(access: ImageAccess) -> Self {
        match access {
            ImageAccess::Read => gl::READ_ONLY,
            ImageAccess::Write => gl::WRITE_ONLY,
            ImageAccess::ReadWrite => gl::READ_WRITE,
        }
    }
}

#[derive(Debug, Copy, Clone, PartialEq, Eq)]
pub enum ImageInternalFormat {
    R8,
    R8SN,
    R8I,
    R8UI,
    R16,
    R16F,
    R16SN,
    R16I,
    R16UI,
    R32F,
    R32I,
    R32UI,

    RG8,
    RG8SN,
    RG8I,
    RG8UI,
    RG16,
    RG16F,
    RG16SN,
    RG16I,
    RG16UI,
    RG32F,
    RG32I,
    RG32UI,

    RGBA8,
    RGBA8SN,
    RGBA8I,
    RGBA8UI,
    RGBA16,
    RGBA16F,
    RGBA16SN,
    RGBA16I,
    RGBA16UI,
    RGBA32F,
    RGBA32I,
    RGBA32UI,

    RGB10A2,
    RGB10A2UI,
    R11FG11FB10F,
}

impl From<ImageInternalFormat> for gl::types::GLenum {
    fn from(internal_format: ImageInternalFormat) -> gl::types::GLenum {
        match internal_format {
            ImageInternalFormat::R8 => gl::R8,
            ImageInternalFormat::R8SN => gl::R8_SNORM,
            ImageInternalFormat::R8I => gl::R8I,
            ImageInternalFormat::R8UI => gl::R8UI,
            ImageInternalFormat::R16 => gl::R16,
            ImageInternalFormat::R16F => gl::R16F,
            ImageInternalFormat::R16SN => gl::R16_SNORM,
            ImageInternalFormat::R16I => gl::R16I,
            ImageInternalFormat::R16UI => gl::R16UI,
            ImageInternalFormat::R32F => gl::R32F,
            ImageInternalFormat::R32I => gl::R32I,
            ImageInternalFormat::R32UI => gl::R32UI,

            ImageInternalFormat::RG8 => gl::RG8,
            ImageInternalFormat::RG8SN => gl::RG8_SNORM,
            ImageInternalFormat::RG8I => gl::RG8I,
            ImageInternalFormat::RG8UI => gl::RG8UI,
            ImageInternalFormat::RG16 => gl::RG16,
            ImageInternalFormat::RG16F => gl::RG16F,
            ImageInternalFormat::RG16SN => gl::RG16_SNORM,
            ImageInternalFormat::RG16I => gl::RG16I,
            ImageInternalFormat::RG16UI => gl::RG16UI,
            ImageInternalFormat::RG32F => gl::RG32F,
            ImageInternalFormat::RG32I => gl::RG32I,
            ImageInternalFormat::RG32UI => gl::RG32UI,

            ImageInternalFormat::RGBA8 => gl::RGBA8,
            ImageInternalFormat::RGBA8SN => gl::RGBA8_SNORM,
            ImageInternalFormat::RGBA8I => gl::RGBA8I,
            ImageInternalFormat::RGBA8UI => gl::RGBA8UI,
            ImageInternalFormat::RGBA16 => gl::RGBA16,
            ImageInternalFormat::RGBA16F => gl::RGBA16F,
            ImageInternalFormat::RGBA16SN => gl::RGBA16_SNORM,
            ImageInternalFormat::RGBA16I => gl::RGBA16I,
            ImageInternalFormat::RGBA16UI => gl::RGBA16UI,
            ImageInternalFormat::RGBA32F => gl::RGBA32F,
            ImageInternalFormat::RGBA32I => gl::RGBA32I,
            ImageInternalFormat::RGBA32UI => gl::RGBA32UI,

            ImageInternalFormat::RGB10A2 => gl::RGB10_A2,
            ImageInternalFormat::RGB10A2UI => gl::RGB10_A2UI,
            ImageInternalFormat::R11FG11FB10F => gl::R11F_G11F_B10F,
        }
    }
}

pub unsafe fn bind_image_texture(
    unit: usize,
    id: TextureId,
    level: usize,
    layer: Option<usize>,
    access: ImageAccess,
    format: ImageInternalFormat,
) -> Result<()> {
    match layer {
        Some(layer) => gl::BindImageTexture(
            unit as u32,
            id.into(),
            level as i32,
            gl::FALSE,
            layer as i32,
            access.into(),
            format.into(),
        ),
        None => gl::BindImageTexture(
            unit as u32,
            id.into(),
            level as i32,
            gl::TRUE,
            0,
            access.into(),
            format.into(),
        ),
    }

    get_error(())
}
