use super::*;
use super::errors::*;

/* vector float uniforms */

pub unsafe fn program_uniform_1f(program: ProgramId, location: usize, v0: f32) -> Result<()> {
    gl::ProgramUniform1f(program.into(), location as i32, v0);

    get_error(())
}

pub unsafe fn program_uniform_2f(program: ProgramId, location: usize, v0: f32, v1: f32) -> Result<()> {
    gl::ProgramUniform2f(program.into(), location as i32, v0, v1);

    get_error(())
}

pub unsafe fn program_uniform_3f(program: ProgramId, location: usize, v0: f32, v1: f32, v2: f32) -> Result<()> {
    gl::ProgramUniform3f(program.into(), location as i32, v0, v1, v2);

    get_error(())
}

pub unsafe fn program_uniform_4f(
    program: ProgramId,
    location: usize,
    v0: f32,
    v1: f32,
    v2: f32,
    v3: f32,
) -> Result<()> {
    gl::ProgramUniform4f(program.into(), location as i32, v0, v1, v2, v3);

    get_error(())
}

/* integer float uniforms */

pub unsafe fn program_uniform_1i(program: ProgramId, location: usize, v0: i32) -> Result<()> {
    gl::ProgramUniform1i(program.into(), location as i32, v0);

    get_error(())
}

pub unsafe fn program_uniform_2i(program: ProgramId, location: usize, v0: i32, v1: i32) -> Result<()> {
    gl::ProgramUniform2i(program.into(), location as i32, v0, v1);

    get_error(())
}

pub unsafe fn program_uniform_3i(program: ProgramId, location: usize, v0: i32, v1: i32, v2: i32) -> Result<()> {
    gl::ProgramUniform3i(program.into(), location as i32, v0, v1, v2);

    get_error(())
}

pub unsafe fn program_uniform_4i(
    program: ProgramId,
    location: usize,
    v0: i32,
    v1: i32,
    v2: i32,
    v3: i32,
) -> Result<()> {
    gl::ProgramUniform4i(program.into(), location as i32, v0, v1, v2, v3);

    get_error(())
}

pub unsafe fn program_uniform_1ui(program: ProgramId, location: usize, v0: u32) -> Result<()> {
    gl::ProgramUniform1ui(program.into(), location as i32, v0);

    get_error(())
}

pub unsafe fn program_uniform_2ui(program: ProgramId, location: usize, v0: u32, v1: u32) -> Result<()> {
    gl::ProgramUniform2ui(program.into(), location as i32, v0, v1);

    get_error(())
}

pub unsafe fn program_uniform_3ui(program: ProgramId, location: usize, v0: u32, v1: u32, v2: u32) -> Result<()> {
    gl::ProgramUniform3ui(program.into(), location as i32, v0, v1, v2);

    get_error(())
}

/* unsigned integer uniforms */

pub unsafe fn program_uniform_4ui(
    program: ProgramId,
    location: usize,
    v0: u32,
    v1: u32,
    v2: u32,
    v3: u32,
) -> Result<()> {
    gl::ProgramUniform4ui(program.into(), location as i32, v0, v1, v2, v3);

    get_error(())
}

/* array of float vector uniforms */

pub unsafe fn program_uniform_1fv(program: ProgramId, location: usize, array: &[f32]) -> Result<()> {
    gl::ProgramUniform1fv(
        program.into(),
        location as i32,
        array.len() as i32,
        array.as_ptr() as *const f32,
    );

    get_error(())
}

pub unsafe fn program_uniform_2fv(program: ProgramId, location: usize, array: &[[f32; 2]]) -> Result<()> {
    gl::ProgramUniform2fv(
        program.into(),
        location as i32,
        array.len() as i32,
        array.as_ptr() as *const f32,
    );

    get_error(())
}

pub unsafe fn program_uniform_3fv(program: ProgramId, location: usize, array: &[[f32; 3]]) -> Result<()> {
    gl::ProgramUniform3fv(
        program.into(),
        location as i32,
        array.len() as i32,
        array.as_ptr() as *const f32,
    );

    get_error(())
}

pub unsafe fn program_uniform_4fv(program: ProgramId, location: usize, array: &[[f32; 4]]) -> Result<()> {
    gl::ProgramUniform4fv(
        program.into(),
        location as i32,
        array.len() as i32,
        array.as_ptr() as *const f32,
    );

    get_error(())
}

/* array of integer vector uniforms */

pub unsafe fn program_uniform_1iv(program: ProgramId, location: usize, array: &[i32]) -> Result<()> {
    gl::ProgramUniform1iv(
        program.into(),
        location as i32,
        array.len() as i32,
        array.as_ptr() as *const i32,
    );

    get_error(())
}

pub unsafe fn program_uniform_2iv(program: ProgramId, location: usize, array: &[[i32; 2]]) -> Result<()> {
    gl::ProgramUniform2iv(
        program.into(),
        location as i32,
        array.len() as i32,
        array.as_ptr() as *const i32,
    );

    get_error(())
}

pub unsafe fn program_uniform_3iv(program: ProgramId, location: usize, array: &[[i32; 3]]) -> Result<()> {
    gl::ProgramUniform3iv(
        program.into(),
        location as i32,
        array.len() as i32,
        array.as_ptr() as *const i32,
    );

    get_error(())
}

pub unsafe fn program_uniform_4iv(program: ProgramId, location: usize, array: &[[i32; 4]]) -> Result<()> {
    gl::ProgramUniform4iv(
        program.into(),
        location as i32,
        array.len() as i32,
        array.as_ptr() as *const i32,
    );

    get_error(())
}

/* array of unsigned integer vector uniforms */

pub unsafe fn program_uniform_1uiv(program: ProgramId, location: usize, array: &[u32]) -> Result<()> {
    gl::ProgramUniform1uiv(
        program.into(),
        location as i32,
        array.len() as i32,
        array.as_ptr() as *const u32,
    );

    get_error(())
}

pub unsafe fn program_uniform_2uiv(program: ProgramId, location: usize, array: &[[u32; 2]]) -> Result<()> {
    gl::ProgramUniform2uiv(
        program.into(),
        location as i32,
        array.len() as i32,
        array.as_ptr() as *const u32,
    );

    get_error(())
}

pub unsafe fn program_uniform_3uiv(program: ProgramId, location: usize, array: &[[u32; 3]]) -> Result<()> {
    gl::ProgramUniform3uiv(
        program.into(),
        location as i32,
        array.len() as i32,
        array.as_ptr() as *const u32,
    );

    get_error(())
}

pub unsafe fn program_uniform_4uiv(program: ProgramId, location: usize, array: &[[u32; 4]]) -> Result<()> {
    gl::ProgramUniform4uiv(
        program.into(),
        location as i32,
        array.len() as i32,
        array.as_ptr() as *const u32,
    );

    get_error(())
}

pub unsafe fn program_uniform_matrix_2f(program: ProgramId, location: usize, array: [f32; 4]) -> Result<()> {
    gl::ProgramUniformMatrix2fv(
        program.into(),
        location as i32,
        1,
        gl::FALSE,
        array.as_ptr() as *const f32,
    );

    get_error(())
}

pub unsafe fn program_uniform_matrix_3f(program: ProgramId, location: usize, array: [f32; 9]) -> Result<()> {
    gl::ProgramUniformMatrix3fv(
        program.into(),
        location as i32,
        1,
        gl::FALSE,
        array.as_ptr() as *const f32,
    );

    get_error(())
}

pub unsafe fn program_uniform_matrix_4f(program: ProgramId, location: usize, array: [f32; 16]) -> Result<()> {
    gl::ProgramUniformMatrix4fv(
        program.into(),
        location as i32,
        1,
        gl::FALSE,
        array.as_ptr() as *const f32,
    );

    get_error(())
}
