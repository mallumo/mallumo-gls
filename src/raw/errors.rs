use super::*;

error_chain! {
    errors {
        InvalidEnum
        InvalidValue
        InvalidOperation
        StackOverflow
        StackUnderflow
        OutOfMemory
        InvalidFramebufferOperation
        ContextLost
        UnknownError
    }
}

pub fn get_error<T>(t: T) -> Result<T> {
    if cfg!(debug_assertions) {
        unsafe {
            let error_code: u32 = gl::GetError();

            use self::ErrorKind::*;
            match error_code {
                gl::INVALID_ENUM => Err(InvalidEnum.into()),
                gl::INVALID_VALUE => Err(InvalidValue.into()),
                gl::INVALID_OPERATION => Err(InvalidOperation.into()),
                gl::STACK_OVERFLOW => Err(StackOverflow.into()),
                gl::STACK_UNDERFLOW => Err(StackUnderflow.into()),
                gl::OUT_OF_MEMORY => Err(OutOfMemory.into()),
                gl::INVALID_FRAMEBUFFER_OPERATION => Err(InvalidFramebufferOperation.into()),
                gl::CONTEXT_LOST => Err(ContextLost.into()),
                gl::NO_ERROR => Ok(t),
                _ => Err(UnknownError.into()),
            }
        }
    } else {
        Ok(t)
    }
}

pub fn get_error_always()  -> Result<()> {
    unsafe {
        let error_code: u32 = gl::GetError();

        use self::ErrorKind::*;
        match error_code {
            gl::INVALID_ENUM => Err(InvalidEnum.into()),
            gl::INVALID_VALUE => Err(InvalidValue.into()),
            gl::INVALID_OPERATION => Err(InvalidOperation.into()),
            gl::STACK_OVERFLOW => Err(StackOverflow.into()),
            gl::STACK_UNDERFLOW => Err(StackUnderflow.into()),
            gl::OUT_OF_MEMORY => Err(OutOfMemory.into()),
            gl::INVALID_FRAMEBUFFER_OPERATION => Err(InvalidFramebufferOperation.into()),
            gl::CONTEXT_LOST => Err(ContextLost.into()),
            gl::NO_ERROR => Ok(()),
            _ => Err(UnknownError.into()),
        }
    }
}