use super::*;
use super::errors::*;

use std::os::raw::c_void;

#[repr(C, packed)]
#[derive(Debug, Copy, Clone, PartialEq, Eq)]
pub struct DrawElementsIndirectCommand {
    pub count: u32,
    pub instance_count: u32,
    pub first_index: u32,
    pub base_vertex: u32,
    pub base_instance: u32,
}

#[repr(C, packed)]
#[derive(Debug, Copy, Clone, PartialEq, Eq)]
pub struct DrawArraysIndirectCommand {
    pub count: u32,
    pub instance_count: u32,
    pub first: u32,
    pub base_instance: u32,
}

#[derive(Debug, Copy, Clone, PartialEq, Eq)]
pub enum DrawMode {
    Points,
    LineStrip,
    LineLoop,
    Lines,
    LineStripAdjacency,
    LinesAdjacency,
    TriangleStrip,
    TriangleFan,
    Triangles,
    TriangleStripAdjacency,
    TrianglesAdjacency,
    Patches,
}

impl From<DrawMode> for gl::types::GLenum {
    fn from(draw_mode: DrawMode) -> gl::types::GLenum {
        match draw_mode {
            DrawMode::Points => gl::POINTS,
            DrawMode::LineStrip => gl::LINE_STRIP,
            DrawMode::LineLoop => gl::LINE_LOOP,
            DrawMode::Lines => gl::LINES,
            DrawMode::LineStripAdjacency => gl::LINE_STRIP_ADJACENCY,
            DrawMode::LinesAdjacency => gl::LINES_ADJACENCY,
            DrawMode::TriangleStrip => gl::TRIANGLE_STRIP,
            DrawMode::TriangleFan => gl::TRIANGLE_FAN,
            DrawMode::Triangles => gl::TRIANGLES,
            DrawMode::TriangleStripAdjacency => gl::TRIANGLE_STRIP_ADJACENCY,
            DrawMode::TrianglesAdjacency => gl::TRIANGLES_ADJACENCY,
            DrawMode::Patches => gl::PATCHES,
        }
    }
}

pub unsafe fn draw_arrays(mode: DrawMode, first: usize, count: usize) -> Result<()> {
    gl::DrawArrays(mode.into(), first as i32, count as i32);

    get_error(())
}

pub unsafe fn draw_arrays_indirect(mode: DrawMode, offset: usize) -> Result<()> {
    gl::DrawArraysIndirect(mode.into(), offset as *const usize as *const c_void);

    get_error(())
}

pub unsafe fn draw_arrays_instanced(mode: DrawMode, first: usize, count: usize, primitive_count: usize) -> Result<()> {
    gl::DrawArraysInstanced(
        mode.into(),
        first as i32,
        count as i32,
        primitive_count as i32,
    );

    get_error(())
}

pub unsafe fn draw_arrays_instanced_base_instace(
    mode: DrawMode,
    first: usize,
    count: usize,
    primitive_count: usize,
    base_instance: usize,
) -> Result<()> {
    gl::DrawArraysInstancedBaseInstance(
        mode.into(),
        first as i32,
        count as i32,
        primitive_count as i32,
        base_instance as u32,
    );

    get_error(())
}

pub unsafe fn multi_draw_arrays(mode: DrawMode, first: &[u32], count: &[u32], draw_count: usize) -> Result<()> {
    gl::MultiDrawArrays(
        mode.into(),
        first.as_ptr() as *const i32,
        count.as_ptr() as *const i32,
        draw_count as i32,
    );

    get_error(())
}

pub unsafe fn multi_draw_arrays_indirect(mode: DrawMode, draw_count: usize, stride: usize) -> Result<()> {
    gl::MultiDrawArraysIndirect(
        mode.into(),
        0 as *const ::std::os::raw::c_void,
        draw_count as i32,
        stride as i32,
    );

    get_error(())
}

pub unsafe fn multi_draw_elements_indirect(mode: DrawMode, draw_count: usize, stride: usize) -> Result<()> {
    gl::MultiDrawElementsIndirect(
        mode.into(),
        gl::UNSIGNED_INT,
        0 as *const ::std::os::raw::c_void,
        draw_count as i32,
        stride as i32,
    );

    get_error(())
}