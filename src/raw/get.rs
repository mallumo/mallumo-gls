use super::*;
use super::errors::*;

use std::mem::uninitialized;

// TODO complete this
pub enum GetParameterName {
    MaxComputeWorkGroupCount,
    MaxComputeWorkGroupSize,
    MaxComputeWorkGroupInvocations,
}

impl From<GetParameterName> for u32 {
    fn from(get_parameter_name: GetParameterName) -> Self {
        use self::GetParameterName::*;
        match get_parameter_name {
            MaxComputeWorkGroupCount => gl::MAX_COMPUTE_WORK_GROUP_COUNT,
            MaxComputeWorkGroupSize => gl::MAX_COMPUTE_WORK_GROUP_SIZE,
            MaxComputeWorkGroupInvocations => gl::MAX_COMPUTE_WORK_GROUP_INVOCATIONS,
        }
    }
}

pub unsafe fn get_integer_i_v(parameter: GetParameterName, index: u32) -> Result<i32> {
    let mut value: gl::types::GLint = uninitialized();

    gl::GetIntegeri_v(parameter.into(), index, &mut value);

    get_error(value)
}
