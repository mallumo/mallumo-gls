use super::*;
use super::errors::*;

pub unsafe fn dispatch_compute(num_groups_x: u32, num_groups_y: u32, num_groups_z: u32) -> Result<()> {
    gl::DispatchCompute(num_groups_x, num_groups_y, num_groups_z);

    get_error(())
}

pub unsafe fn dispatch_compute_indirect(buffer_offset: usize) -> Result<()> {
    gl::DispatchComputeIndirect(buffer_offset as isize);

    get_error(())
}

#[repr(C, packed)]
#[derive(Debug, Copy, Clone)]
pub struct DispatchCommand {
    pub num_groups_x: u32,
    pub num_groups_y: u32,
    pub num_groups_z: u32,
}
