//! Raw module wraps C OpenGL functions for convenience, it does following
//! things:
//!
//! * OpenGL enums, which are integers, are wrapped in Rusts enums
//!   avoiding `GL_INVALID_ENUM` errors
//! * Hides C pointers and returns only safe Rust types as results. For example
//!   `get_program_log` returns String, `create_buffer` does not take anything and
//!   returns newtype `BufferId`
//!
//! Not all of OpenGL functions are wrapped, only those that were picked as
//! suitable for modern programming. More specifically they are limited to
//! OpenGL 4.5 Core version + few extensions beyond 4.5. Futhermore they
//! are also limited by techniques which should be preferred nowadays for
//! performance reasons:
//!
//! * only Direct State Access functions
//! * no Vertex Array Object, you should use Full Vertex Pulling instead
//!   (see OpenGL Insights)
//! * only bindless textures
//!
//! Collection of functions was inspired by
//! [this list](https://docs.google.com/spreadsheets/d/1CZ3K0I_08L6CuSHAxikB56WhPJPdj8yy9RqLexK4fgs)
//!

#[allow(non_upper_case_globals)]
#[doc(hidden)]
#[cfg_attr(feature = "cargo-clippy", allow(clippy))]
pub mod gl {
    include!(concat!(env!("OUT_DIR"), "/bindings.rs"));
}

mod errors;
pub use self::errors::{get_error, get_error_always};

mod buffer;
pub use self::buffer::*;

mod compute;
pub use self::compute::*;

mod shader;
pub use self::shader::*;

mod program;
pub use self::program::*;

mod shader_program;
pub use self::shader_program::*;

mod pipeline;
pub use self::pipeline::*;

mod texture;
pub use self::texture::*;

mod image;
pub use self::image::*;

mod framebuffer;
pub use self::framebuffer::*;

mod sync;
pub use self::sync::*;

mod state;
pub use self::state::*;

mod get;
pub use self::get::*;

mod draw;
pub use self::draw::*;

mod uniform;
pub use self::uniform::*;

#[derive(Debug, Copy, Clone, PartialEq, Eq)]
pub enum Face {
    Front,
    Back,
    FrontBack,
}

impl From<Face> for u32 {
    fn from(face: Face) -> Self {
        match face {
            Face::Front => gl::FRONT,
            Face::Back => gl::BACK,
            Face::FrontBack => gl::FRONT_AND_BACK,
        }
    }
}

impl Default for Face {
    fn default() -> Face {
        Face::Back
    }
}
