use super::Buffer;
use super::errors::*;

use std::mem::size_of;
use std::os::raw::c_void;

use raw::{create_buffer, delete_buffer, map_named_buffer_range, named_buffer_storage, BufferData, BufferId, BufferMap,
          BufferMutability};

#[derive(Debug)]
pub struct MappedMutableBuffer {
    id: BufferId,
}

impl MappedMutableBuffer {
    /// Creates new buffer and copies `data` to its memory on GPU
    ///
    /// # Errors
    /// - GL_OUT_OF_MEMORY - if the buffer cannot be created
    ///
    pub fn new<T>(data: &[T]) -> Result<MappedMutableBuffer> {
        if data.len() * size_of::<T>() == 0 {
            bail!("Buffer size must be greater than 0");
        }

        let id = create_buffer();

        unsafe {
            named_buffer_storage(
                id,
                BufferData::Data(data),
                BufferMutability::Mutable,
                BufferMap::ReadWrite,
            ).chain_err(|| ErrorKind::BufferCreationError)?;
        }

        Ok(MappedMutableBuffer { id: id })
    }

    /// Creates new empty buffer
    ///
    /// # Errors
    /// - BufferCreationError(GL_OUT_OF_MEMORY) - if the buffer cannot be created
    ///
    pub fn new_empty(size: usize) -> Result<MappedMutableBuffer> {
        if size == 0 {
            bail!("Buffer size must be greater than 0");
        }

        let id = create_buffer();

        unsafe {
            named_buffer_storage::<u8, Option<BufferMap>>(
                id,
                BufferData::Empty(size),
                BufferMutability::Mutable,
                Some(BufferMap::ReadWrite),
            ).chain_err(|| ErrorKind::BufferCreationError)?;
        }

        Ok(MappedMutableBuffer { id: id })
    }

    pub unsafe fn get_pointer(&self, length: usize, offset: usize) -> Result<*mut c_void> {
        let pointer: *mut c_void = map_named_buffer_range(self.get_id(), offset, length, BufferMap::ReadWrite)
            .chain_err(|| ErrorKind::BufferGetPointerError)?;
        Ok(pointer)
    }
}

impl Buffer for MappedMutableBuffer {
    fn get_id(&self) -> BufferId {
        self.id
    }
}

impl Drop for MappedMutableBuffer {
    fn drop(&mut self) {
        unsafe {
            delete_buffer(self.id);
        }
    }
}
