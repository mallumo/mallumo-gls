error_chain! {
    errors {
        BufferCreationError
        BufferSetSubDataError
        BufferCopyError
        BufferClearError
        BufferGetPointerError
    }
}
