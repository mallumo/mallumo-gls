use super::Buffer;
use super::errors::*;

use std::mem::size_of;

use raw::*;

#[derive(Debug)]
pub struct MutableBuffer {
    id: BufferId,
    size: usize,
}

impl MutableBuffer {
    /// Creates new buffer and copies `data` to its memory on GPU
    ///
    /// # Errors
    /// - GL_OUT_OF_MEMORY - if the buffer cannot be created
    ///
    pub fn new<T>(data: &[T]) -> Result<MutableBuffer> {
        if data.len() * size_of::<T>() == 0 {
            bail!("Buffer size must be greater than 0");
        }

        let id = create_buffer();

        unsafe {
            named_buffer_storage(id, BufferData::Data(data), BufferMutability::Mutable, None)
                .chain_err(|| ErrorKind::BufferCreationError)?;
        }

        Ok(MutableBuffer {
            id: id,
            size: size_of::<T>() * data.len(),
        })
    }

    /// Creates new empty buffer
    ///
    /// # Errors
    /// - BufferCreationError(GL_OUT_OF_MEMORY) - if the buffer cannot be created
    ///
    pub fn new_empty(size: usize) -> Result<MutableBuffer> {
        if size == 0 {
            bail!("Buffer size must be greater than 0");
        }

        let id = create_buffer();

        unsafe {
            named_buffer_storage::<u8, Option<BufferMap>>(id, BufferData::Empty(size), BufferMutability::Mutable, None)
                .chain_err(|| ErrorKind::BufferCreationError)?;
        }

        Ok(MutableBuffer { id: id, size: size })
    }

    pub fn new_zeroed(size: usize) -> Result<MutableBuffer> {
        let mut buffer = MutableBuffer::new_empty(size).chain_err(|| "Could not create empty buffer")?;
        buffer.clear_data().chain_err(|| "Could not clear buffer")?;
        Ok(buffer)
    }

    pub fn set_data<T>(&mut self, data: &[T]) -> Result<()> {
        unsafe {
            named_buffer_sub_data(self.id, 0, data).chain_err(|| ErrorKind::BufferSetSubDataError)?;
        }

        Ok(())
    }

    /// Replaces part of memory with `data`
    ///
    /// # Errors
    /// - BufferSetSubDataError(GL_INVALID_VALUE) - if size of `data` + offset > buffer size
    ///
    pub fn set_sub_data<T>(&mut self, data: &[T], offset: usize) -> Result<()> {
        unsafe {
            named_buffer_sub_data(self.id, offset, data).chain_err(|| ErrorKind::BufferSetSubDataError)?;
        }

        Ok(())
    }

    pub fn clear_data(&mut self) -> Result<()> {
        let empty = vec![0u8; self.size];

        self.set_sub_data(&empty, 0)
            .chain_err(|| "Could not clear data")?;

        Ok(())
    }

    pub fn copy_to(
        &mut self,
        other: &mut MutableBuffer,
        read_offset: usize,
        write_offset: usize,
        size: usize,
    ) -> Result<()> {
        unsafe {
            copy_named_buffer_sub_data(self.id, other.id, read_offset, write_offset, size)
                .chain_err(|| ErrorKind::BufferCopyError)?;
        }

        Ok(())
    }

    pub fn get<T>(&self, size: usize, offset: usize) -> Result<Vec<T>> {
        if size_of::<T>() * (size + offset) > self.size {
            bail!("Trying to read out of bounds");
        }

        let mut result = Vec::with_capacity(size);
        unsafe {
            get_named_buffer_sub_data(
                self.id,
                size_of::<T>() * offset,
                size_of::<T>() * size,
                result.as_mut_slice(),
            ).chain_err(|| "Could not get buffer data")?;

            result.set_len(size);
        }

        Ok(result)
    }

    pub fn resize(&mut self, size: usize) -> Result<()> {
        let new_id = create_buffer();

        unsafe {
            named_buffer_storage::<u8, Option<BufferMap>>(
                new_id,
                BufferData::Empty(size),
                BufferMutability::Mutable,
                None,
            ).chain_err(|| ErrorKind::BufferCreationError)?;
        }

        unsafe {
            copy_named_buffer_sub_data(self.id, new_id, 0, 0, size).chain_err(|| ErrorKind::BufferCopyError)?;
        }

        unsafe {
            delete_buffer(self.id);
        }

        self.id = new_id;

        Ok(())
    }
}

impl Buffer for MutableBuffer {
    fn get_id(&self) -> BufferId {
        self.id
    }
}

impl Drop for MutableBuffer {
    fn drop(&mut self) {
        unsafe {
            delete_buffer(self.id);
        }
    }
}
