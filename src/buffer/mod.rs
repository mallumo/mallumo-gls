mod errors;
use errors::*;

mod immutable_buffer;
pub use self::immutable_buffer::*;

mod mutable_buffer;
pub use self::mutable_buffer::*;

mod mapped_mutable_buffer;
pub use self::mapped_mutable_buffer::*;

use raw::*;

pub trait Buffer {
    fn get_id(&self) -> BufferId;

    fn bind_base(&self, target: BufferBaseTarget, index: usize) -> Result<()> {
        unsafe {
            bind_buffer_base(target, BlockIndex(index as u32), self.get_id()).chain_err(|| "Could not bind buffer")?;
        }

        Ok(())
    }

    fn bind_range(&self, target: BufferBaseTarget, index: usize, offset: usize, size: usize) -> Result<()> {
        unsafe {
            bind_buffer_range(
                target,
                BlockIndex(index as u32),
                self.get_id(),
                offset,
                size,
            ).chain_err(|| "Could not bind buffer range")?;
        }

        Ok(())
    }

    fn bind_as_indirect(&self) -> Result<()> {
        unsafe {
            bind_buffer(BindBufferTarget::DrawIndirectBuffer, self.get_id())
                .chain_err(|| "Could not bind draw indirect buffer")?;
        }

        Ok(())
    }
}
